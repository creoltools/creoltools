(*
 * Location.ml -- Tracking source code locations
 *
 * This file is part of creoltools
 *
 * Written and Copyright (c) 2010 by Marcel Kyas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)


(** This module defines a structure for the location of abstract
    syntax nodes in a file.  *)


(* Use sexplib *)
open Sexplib
open Sexp
open Conv
TYPE_CONV_PATH "CreolSexp"

type t = {
  file: string;                (** The file in which the node occurs. *)
  line: int        (** The line within file in which the node occurs. *)
}
with sexp

let make file line = { file = Filename.basename file; line = line }

let dummy = { file = ""; line = 0 }

let dummy_p = function
    | { file = ""; line = 0 } -> true
    | _ -> false

let file { file = result } = result

let line { line = result } = result

let string_of_location ({ file = file; line = line } as loc) =
  if dummy_p loc then
    "<internal>"
  else
    (file ^ ":" ^ (string_of_int line))
