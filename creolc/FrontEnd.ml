(*
 * FrontEnd.ml -- Parsing and preprocessing input files.
 *
 * This file is part of creoltools
 *
 * Written and Copyright (c) 2007-2010 by Marcel Kyas
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *)

open Creol

let cpp = ref Version.preproc

let cpp_args : string list ref = ref []

let cpp_define arg = cpp_args := (!cpp_args)@["-D" ^ arg]

let cpp_undefine arg = cpp_args := (!cpp_args)@["-U" ^ arg]

let cpp_include arg = cpp_args := (!cpp_args)@["-I" ^ arg]


(** Read the contents from a channel and return a abstract syntax tree
    and measure the time used for it.  *)
let do_parse_from_channel main name channel =
  let () = Messages.message 1 ("Parsing " ^ name) in
  let buf = Lexing.from_channel channel in
  let pos = buf.Lexing.lex_curr_p in
  let () =  buf.Lexing.lex_curr_p <- { pos with Lexing.pos_fname = name } in
  let do_parse () = Program.make (main CreolLexer.token buf) in
  let (result, elapsed) = Misc.measure do_parse in
  let () = Passes.time_parse := !Passes.time_parse +. elapsed in
    result

let preprocess_and_parse_pipe do_parse name in_descr args =
  let cpp' = Str.split (Str.regexp "[ \t]+") !cpp in
  let args' = cpp'@args in
  let () =
    Messages.message 1 ("Preprocessing " ^ name ^ " with command `" ^
      (String.concat " " args') ^ "'")
  in
  let (input, output) = Unix.pipe () in
  let channel = Unix.in_channel_of_descr input in
  let pid =
    Unix.create_process (List.hd cpp') (Array.of_list args')
      in_descr output Unix.stderr
  in
  let () = Unix.close output in
  let result = do_parse name channel in
  let () = Unix.close input in
  (* let (_, _) = Unix.waitpid [] pid in *)
    result

let preprocess_and_parse do_parse name in_descr args =
  let tmpname = Filename.temp_file "creolc" ".tmp" in
  let args' = (!cpp)::"-o"::tmpname::args in
  let () =
    Messages.message 1 ("Preprocessing " ^ name ^ " with command " ^
      (String.concat " " args'))
  in
  let pid =
    Unix.create_process (!cpp) (Array.of_list args')
      in_descr Unix.stdout Unix.stderr
  in
  let (_, _) = Unix.waitpid [] pid in
  let result =
    try
      do_parse name (open_in tmpname)
    with x ->
      Unix.unlink tmpname;
      raise x
  in
    Unix.unlink tmpname;
    result


(**  Parse a file from a channel, assuming that it is called stdin. *)
let parse_from_channel main channel =
  let input_descr = Unix.descr_of_in_channel channel 
  and do_parse = do_parse_from_channel main in
    preprocess_and_parse_pipe do_parse "<stdin>" input_descr (!cpp_args)




(** Read the contents of a file and return an abstract syntax tree.  *)
let parse_from_file main name =
  let file =
    if (Sys.file_exists name) || (String.contains name '/') then
      name
    else
      try
        Misc.find_file_in_path (Config.get_library_path ()) name
      with
          Not_found -> name
  and do_parse = do_parse_from_channel main
  in
    preprocess_and_parse_pipe do_parse name Unix.stdin (file::!cpp_args)


(** Read the contents of a list of files and return an abstract syntax
    tree.  *)
let parse_from_files main files =
  Program.concat (List.map (parse_from_file main) files)
