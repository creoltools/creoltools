## Makefile.am - Use automake to create Makefile.in
#
# This file is part of creoltools
#
# Written and Copyright (c) 2007, 2008 by Marcel Kyas
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

# Subdirs
MOSTLYCLEANFILES	= *~ *.aux *.log *.out *.toc \
			  *.cma *.cmxa *.cmi *.cmo *.cmx *.a *.o
CLEANFILES		= creolc.prof
DISTCLEANFILES		= .depend
MAINTAINERCLEANFILES	= $(BUILT_SOURCES) $(srcdir)/Makefile.in

MENHIR_INPUTS		= $(srcdir)/CreolLexer.mll \
			  $(srcdir)/CreolTokens.mly \
			  $(srcdir)/CreolCommon.mly \
			  $(srcdir)/CreolParser.mly \
			  $(srcdir)/UpdateParser.mly

OCAML_INPUTS		= $(srcdir)/CreolTokens.mli $(srcdir)/CreolTokens.ml \
  			  $(srcdir)/CreolLexer.mli $(srcdir)/CreolLexer.ml \
  			  $(srcdir)/CreolParser.mli $(srcdir)/CreolParser.ml \
  			  $(srcdir)/UpdateParser.mli $(srcdir)/UpdateParser.ml \
			  $(srcdir)/CMCParser.ml \
  			  $(srcdir)/Passes.mli $(srcdir)/Passes.ml \
  			  $(srcdir)/FrontEnd.ml \
  			  $(srcdir)/Driver.ml \
			  $(srcdir)/UpdateCompiler.ml \
			  $(srcdir)/CMCDisplay.ml

EXTRA_DIST		= $(MENHIR_INPUTS) $(OCAML_INPUTS)

BUILT_SOURCES		= $(srcdir)/CreolLexer.ml \
			  $(srcdir)/CreolTokens.ml $(srcdir)/CreolTokens.mli \
			  $(srcdir)/CreolParser.ml $(srcdir)/CreolParser.mli \
			  $(srcdir)/UpdateParser.ml $(srcdir)/UpdateParser.mli

EXTRA_PROGRAMS		= creolc creolc.opt \
			  creolupdc creolupdc.opt \
			  cmcdisplay cmcdisplay.opt

bin_PROGRAMS		= $(CREOLC) $(CREOLC_OPT) \
			  $(CREOLUPDC) $(CREOLUPDC_OPT) \
			  $(CMCDISPLAY) $(CMCDISPLAY_OPT)

creolc_SOURCES		=

creolc_OBJ		= CreolTokens.cmi CreolTokens.cmo \
			  CreolLexer.cmi CreolLexer.cmo \
			  CreolParser.cmi CreolParser.cmo \
			  UpdateParser.cmi UpdateParser.cmo \
			  Passes.cmi Passes.cmo \
			  FrontEnd.cmo \
			  Driver.cmo

creolc_opt_SOURCES	=

creolc_opt_OBJ		= CreolTokens.cmi CreolTokens.cmx \
			  CreolLexer.cmi CreolLexer.cmx \
			  CreolParser.cmi CreolParser.cmx \
			  UpdateParser.cmi UpdateParser.cmx \
			  Passes.cmi Passes.cmx \
			  FrontEnd.cmx \
			  Driver.cmx

creolupdc_SOURCES	= 

creolupdc_OBJ		= CreolTokens.cmi CreolTokens.cmo \
			  CreolLexer.cmi CreolLexer.cmo \
			  CreolParser.cmi CreolParser.cmo \
			  UpdateParser.cmi UpdateParser.cmo \
			  CMCParser.cmo \
			  Passes.cmi Passes.cmo \
			  FrontEnd.cmo \
			  UpdateCompiler.cmo

creolupdc_opt_SOURCES	=

creolupdc_opt_OBJ	= CreolTokens.cmi CreolTokens.cmx \
			  CreolLexer.cmi CreolLexer.cmx \
			  CreolParser.cmi CreolParser.cmx \
			  UpdateParser.cmi UpdateParser.cmx \
			  CMCParser.cmx \
			  Passes.cmi Passes.cmx \
			  FrontEnd.cmx \
			  UpdateCompiler.cmx

cmcdisplay_SOURCES	=

cmcdisplay_opt_SOURCES	= $(cmcdisplay_SOURCES)

OCAMLCFLAGS		+= -g
OCAMLOPTFLAGS		+= -w a -noassert -unsafe -inline 20
OCAMLFINDFLAGS		+= -package "num unix str sexplib.syntax creol"

export OCAMLPATH

# The compilers.
OCAMLC		= $(OCAMLFIND) ocamlc -I $(srcdir) -I .
OCAMLCP		= $(OCAMLFIND) ocamlcp -I $(srcdir) -I .
OCAMLOPT	= $(OCAMLFIND) ocamlopt -I $(srcdir) -I .
OCAMLDEP	= $(OCAMLFIND) ocamldep -I ../creol -I $(srcdir) -I .
OCAMLDOC	= $(OCAMLFIND) ocamldoc -I $(srcdir) -I .

SUFFIXES = .a .ml .mli .mll .mly .cma .cmi .cmo .cmx .cmxa

creolc: $(creolc_OBJ)
	{ f=`echo $^ | sed 's,[^[:blank:]]*\.cmi,,g'`; } ; \
	  $(OCAMLC) $(OCAMLFINDFLAGS) $(OCAMLCFLAGS) -linkpkg -o $@ $$f

$(creolc_OBJ): ../creol/creol.cma

creolc.opt: $(creolc_opt_OBJ)
	{ f=`echo $^ | sed 's,[^[:blank:]]*\.cmi,,g'`; } ; \
	  $(OCAMLOPT) $(OCAMLFINDFLAGS) $(OCAMLOPTFLAGS) -linkpkg -o $@ $$f

$(creolc_opt_OBJ): ../creol/creol.cmxa

creolupdc: $(creolupdc_OBJ)
	{ f=`echo $^ | sed 's,[^[:blank:]]*\.cmi,,g'`; } ; \
	  $(OCAMLC) $(OCAMLFINDFLAGS) $(OCAMLCFLAGS) -linkpkg -o $@ $$f

$(creolupdc_OBJ): ../creol/creol.cma

creolupdc.opt: $(creolupdc_opt_OBJ)
	{ f=`echo $^ | sed 's,[^[:blank:]]*\.cmi,,g'`; } ; \
	  $(OCAMLOPT) $(OCAMLFINDFLAGS) $(OCAMLOPTFLAGS) -linkpkg -o $@ $$f

$(creolupdc_opt_OBJ): ../creol/creol.cmxa

cmcdisplay: $(cmcdisplay_OBJ)
	{ f=`echo $^ | sed 's,[^[:blank:]]*\.cmi,,g'`; } ; \
	  $(OCAMLC) $(OCAMLFINDFLAGS) $(OCAMLCFLAGS) -linkpkg -o $@ $$f

$(cmcdisplay_OBJ): ../creol/creol.cma

cmcdisplay.opt: $(cmcdisplay_opt_OBJ)
	{ f=`echo $^ | sed 's,[^[:blank:]]*\.cmi,,g'`; } ; \
	  $(OCAMLOPT) $(OCAMLFINDFLAGS) $(OCAMLOPTFLAGS) -linkpkg -o $@ $$f

$(cmcdisplay_opt_OBJ): ../creol/creol.cmxa

# These generic rules compile an OCaml source file to an object file.
.PRECIOUS: *.cmo *.cmi *.cmx *.cma *a. *.cmxa
.ml.cmo:
	$(OCAMLC) $(OCAMLFINDFLAGS) $(OCAMLCFLAGS) -c $< -o $@

.mli.cmi:
	$(OCAMLC) $(OCAMLFINDFLAGS) $(OCAMLCFLAGS) -c $< -o $@ 

.ml.cmx:
	$(OCAMLOPT) $(OCAMLFINDFLAGS) $(OCAMLOPTFLAGS) -c $< -o $@

$(srcdir)/CreolLexer.ml: $(srcdir)/CreolLexer.mll
	$(OCAMLLEX) -o $@ $<

# Compile the token definitions.
$(srcdir)/CreolTokens.ml:  $(srcdir)/CreolTokens.mli
	@if test -f $@ ; \
	then \
	    touch $@ ; \
	else \
	    rm -f $(srcdir)/CreolTokens.mli ; \
	    $(MAKE) $(AM_MAKEFLAGS) $(srcdir)/CreolTokens.mli ; \
	fi

$(srcdir)/CreolTokens.mli: $(srcdir)/CreolTokens.mly
	$(MENHIR) $(MENHIR_FLAGS) --only-tokens \
	    --base $(srcdir)/CreolTokens $(srcdir)/CreolTokens.mly

# The code that runs menhir with type inference became much too
# fragile. 
# We must make sure that Creol.cmi and Mesages.cmi are built before
# CreolParser.ml or UpdateParser.ml, because we use the --infer flag
# during the compilation of them.  We do not want that CreolParser.ml
# depends on Creol.cmi or Messages.cmi, because then compiling the
# project at the end user requires recompilation of CreolParser.ml,
# which only works if the end user has menhir installed.
#
# Nonetheless, we depend on the source files, since the parser should
# be checked when these headers change.
#
$(srcdir)/CreolParser.ml: $(srcdir)/CreolParser.mly
	rm -f CreolParser.conflicts $@
	$(MENHIR) $(MENHIR_FLAGS) \
	    --infer --ocamlc '$(OCAMLC) $(OCAMLFINDFLAGS) $(OCAMLCFLAGS)' \
	    --external-tokens CreolTokens \
	    --base $(srcdir)/CreolParser \
	    $(srcdir)/CreolTokens.mly $(srcdir)/CreolCommon.mly \
	    $(srcdir)/CreolParser.mly
	(t=`mktemp menhir.XXXXXXXX` && mv $(srcdir)/CreolParser.ml $$t && \
	sed 's,_menhir_shifted = [0123456789][0123456789]*\(.*\)$$,_menhir_shifted = max_int \1,g' $$t > $(srcdir)/CreolParser.ml && \
	rm $$t)

$(srcdir)/CreolParser.mli: $(srcdir)/CreolParser.ml
	@if test -f $@ ; \
	then \
	    touch $@ ; \
	else \
	    rm -f $(srcdir)/CreolParser.ml ; \
	    $(MAKE) $(AM_MAKEFLAGS) $(srcdir)/CreolParser.ml ; \
	fi

$(srcdir)/UpdateParser.ml: $(srcdir)/UpdateParser.mly
	rm -f UpdateParser.conflicts
	$(MENHIR) $(MENHIR_FLAGS) --external-tokens CreolTokens \
	    --infer --ocamlc '$(OCAMLC) $(OCAMLFINDFLAGS) $(OCAMLCFLAGS)' \
	    --base $(srcdir)/UpdateParser \
	    $(srcdir)/CreolTokens.mly $(srcdir)/CreolCommon.mly \
	    $(srcdir)/UpdateParser.mly
	(t=`mktemp menhir.XXXXXXXX` && mv $(srcdir)/UpdateParser.ml $$t && \
	sed 's,_menhir_shifted = [0123456789][0123456789]*\(.*\)$$,_menhir_shifted = max_int \1,g' $$t > $(srcdir)/UpdateParser.ml && \
	rm $$t)

$(srcdir)/UpdateParser.mli: $(srcdir)/UpdateParser.ml
	@if test -f $@ ; \
	then \
	    touch $@ ; \
	else \
	    rm -f $(srcdir)/UpdateParser.ml ; \
	    $(MAKE) $(AM_MAKEFLAGS) $(srcdir)/UpdateParser.ml ; \
	fi

distclean-local:
	test ! -d html || rm -rf html
	test ! -f html-stamp || rm -f html-stamp

# Libraries
../creol/creol.cma ../creol/creol.cmxa:
	make -C ../creol all

# Compute the dependencies
#
.depend: Makefile $(MENHIR_INPUTS) $(OCAML_INPUTS)
	$(OCAMLDEP) $(OCAMLFINDFLAGS) $(OCAMLDEPFLAGS) $^ > $@
	{ t=`echo $(srcdir) | sed s,\\\.,\\\\\\\.,g` ; sed "s,$$t/,,g" -i $@ ; }

# Dependencies.
CreolParser.cmi: CreolParser.mli ../creol/Creol.cmi
UpdateParser.cmi: UpdateParser.mli ../creol/Creol.cmi
CreolLexer.cmi: CreolTokens.cmi
CreolLexer.cmo CreolLexer.cmx: CreolLexer.cmi
CMCParser.cmo CMCParser.cmx: ../creol/Creol.cmi
Passes.cmi: ../creol/Creol.cmi

$(srcdir)/CreolParser.ml $(srcdir)/UpdateParser.ml: $(srcdir)/CreolTokens.mli
$(srcdir)/CreolParser.ml $(srcdir)/UpdateParser.ml: $(srcdir)/CreolTokens.mly
$(srcdir)/CreolParser.ml $(srcdir)/UpdateParser.ml: $(srcdir)/CreolCommon.mly

-include .depend


# Count lines.
#
# Parameters are for a semi-detached project
# (Effort 3.0 1.12, Schedule 2.5 0.38)
#
# RELY Low 0.88
# DATA Low 0.94
# CPLX Nominal 1.0
# TIME Nominal 1.0
# STOR Nominal 1.0
# VIRT Nominal 1.0
# TURN Nominal 1.0
# ACAP Low 1.19
# AEXP Low 1.13
# PCAP Low 1.17
# VEXP High 0.9
# LEXP High 0.95
# MODP Very High 0.82
# TOOL High 0.91
# SCED High 1.04
#
# Total: 0.86
#
sloccount:
	@if test "$(srcdir)" != "." ; then \
	  for i in $^ ; do cp $$i $(srcdir)/$$i ; done ; \
	fi
	@sloccount --effort 2.59 1.12 --schedule 2.5 0.35 --personcost 424000 \
	  $(srcdir)
	@if test "$(srcdir)" != "." ; then \
	  for i in $^ ; do rm $(srcdir)/$$i ; done ; \
	fi
